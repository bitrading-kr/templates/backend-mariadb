#!/bin/bash

# Install backend
rm -rf backend/node_modules
rm -f backend/package-lock.json
npm install --prefix ./backend

# Add .env variables
cp .env-example .env

# Deploy docker-compose
docker-compose up --build -d
